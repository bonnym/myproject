from django.http import HttpResponse
from django.shortcuts import render,redirect, get_object_or_404
from django.template import loader
from django.views.generic import View

from .models import *



def index(request):
    template = loader.get_template('polls/index.html')
    context = {
        'youtubes':Youtube.objects.all(),
    }
    return HttpResponse(template.render(context, request))

def contact(request):
    return render(request, 'polls/contact.html')

def crew(request):
    context = {
        'crews': Crew.objects.all(),
    }
    return render(request, 'polls/crew.html', context)

#
# def detail(request, crew_id):
#     crew = get_object_or_404(Crew, pk=crew_id)
#     return render(request, 'photo/photo_detail.html', {'crew': crew})


def reels(request):
    return render(request, 'polls/reels.html')