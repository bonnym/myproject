from django.contrib import admin
from .models import *


class CrewModelAdmin(admin.ModelAdmin):
    list_display = ["__str__", "updated", "timestamp"]

    class Meta:
        model = Crew

admin.site.register(Crew)
admin.site.register(Youtube)
