from django.conf.urls import url
from . import views

app_name = 'polls'

urlpatterns = [

    url(r'^$', views.index, name='index'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^crew$', views.crew, name='crew'),
    # url(r'^(?P<crew_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^reels$', views.reels, name='reels'),

]