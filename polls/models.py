from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.contrib.auth.models import Permission, User
from django.db import models
from embed_video.fields import EmbedVideoField

class Crew(models.Model):
    name = models.CharField(max_length=200)
    avator = models.ImageField(null=True, blank=True)
    designation = models.CharField(max_length=200, null=True)
    other = models.CharField(max_length=200, null=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("polls:crew", kwargs={"id": self.id})

    class Meta:
        ordering = ['name']


class Youtube(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True,)
    link = EmbedVideoField()
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Youtubes'
        ordering = ['name']
