from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()


urlpatterns = [
                  url(r'^admin/', admin.site.urls),
                  url(r'^', include('polls.urls', namespace="polls")),
                  url(r'^', include('video.urls', namespace="video")),
                  url(r'^', include('talent.urls', namespace="talent")),
                  url(r'^', include('photo.urls', namespace="photo")),
              ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
