from django.conf.urls import url
from django.contrib import admin

from . import views

app_name = 'video'



urlpatterns = [
    # url(r'^(?P<pk>\d+)/$', views.DetailView.as_view(), name='video-detail'),
    url(r'^(?P<pk>\d+)/$', views.DetailView, name='video-detail'),
    url(r'^season/$', views.IndexView.as_view(), name='season'),
]
