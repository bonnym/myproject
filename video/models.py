from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.db import models
from embed_video.fields import EmbedVideoField


def upload_location(instance, filename):
    return "%s/%s" %(instance.id, filename)





class Season(models.Model):
    season_name = models.CharField(max_length=250)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    editor = models.CharField(max_length=70)
    cover = models.ImageField(null=True, blank=True)
    slug = models.SlugField(unique=True)


    def __str__(self):
        return self.season_name

    def get_absolute_url(self):
        return reverse("video:video-detail", kwargs={"slug":self.slug})

    class Meta:
        verbose_name_plural = 'Seasons'
        ordering = ['season_name']

class Video(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True,)
    # link = models.URLField(max_length=500, blank=True, default='')
    link = EmbedVideoField()
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    slug = models.SlugField(unique=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("video:video-detail", kwargs={"slug":self.slug})

    class Meta:
        verbose_name_plural = 'Videos'
        ordering = ['name']


# class Video(models.Model):
#     name = models.CharField(max_length=200,)
#     file_path = models.FileField(null=True, blank=True, upload_to='video')
#     season = models.ForeignKey(Season, on_delete=models.CASCADE)
#     description = models.TextField()
#     thumbnail = models.ImageField(null=True, blank=True)
#     created = models.DateTimeField(auto_now_add=True)
#     slug = models.SlugField(unique=True)
#
#
#     def __str__(self):
#         return self.name
#
#     def get_absolute_url(self):
#         return reverse("video:video-detail", kwargs={"slug":self.slug})
#
#     class Meta:
#         verbose_name_plural = 'Videos'
#         ordering = ['name', ]

def create_slug(instance, new_slug):
    slug = slugify(instance.name)
    if new_slug is not None:
        slug = new_slug
    qs = Video.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_video_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)

        # pre_save.connect(pre_save_video_receiver, sender=Video)