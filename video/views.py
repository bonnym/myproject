from django.conf.urls import url
from django.shortcuts import get_object_or_404, render
from django.views.generic import DetailView
from django.views import generic

from .models import Season, Video

VIDEO_FILE_TYPES = ['mp4']
IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']



class IndexView(generic.ListView):
    template_name ='video/season.html'

    def get_queryset(self):
        return Season.objects.all()



# class DetailView(generic.DetailView):
#     model = Season
#     template_name = 'video/video.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(DetailView, self).get_context_data(**kwargs)
#         context['video'] = Video.objects.get(id=pk)
#         return context

def DetailView(request, pk):
    video = get_object_or_404(Video, pk=pk)
    return render(request, 'video/video.html', {'video': video})