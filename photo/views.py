from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Album, Pin
from .forms import AlbumForm, PinForm, UserForm
from django.db.models import Q
from django.http import JsonResponse
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.views.generic import DetailView, ListView, CreateView


IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']



def pin_create(request):
    form = PinForm()
    context = {
        "form": form,
    }
    return render(request, "photo/pin_form.html", context)

def album(request):
    context = {
        'albums': Album.objects.all(),
    }
    return render(request, 'photo/album.html', context)


# def detail(request, album_id):
#     album = get_object_or_404(Album, pk=album_id)
#     return render(request, 'photo/photo_detail.html', {'album': album})


def show(request, pk):
    album = get_object_or_404(Album, pk=slug)
    template = 'photo/show.html'
    return render(request, template, {'album': album})




def Form(request):
    return render(request, 'photo/pin_form.html')


def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'photo/login_user.html', context)

def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                albums = Album.objects.filter(user=request.user)
                return render(request, 'polls/index.html', {'albums': albums})
            else:
                return render(request, 'photo/login_user.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'photo/login_user.html', {'error_message': 'Invalid login'})
    return render(request, 'photo/login_user.html')

def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                albums = Album.objects.filter(user=request.user)
                return render(request, 'polls/index.html', {'albums': albums})
    context = {
        "form": form,
    }
    return render(request, 'photo/register.html', context)

def pin_update(request, id=None):
    instance = get_object_or_404(Pin, id=id)
    form = PinForm(request.POST or None, request.FILES or None, instance=instance)
    if form.if_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "<a href='#'> Item</a> Saved", extra_tags='html_safe')
        return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        "name" : name,
        "instance": instance,
        "form" : form,
    }
    return render(request, "pin_form.html", context)



