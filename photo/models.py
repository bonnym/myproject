from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import Permission, User




def upload_location(instance, filename):
    return "%s/%s" %(instance.id, filename)



class Album(models.Model):
    user = models.ForeignKey(User, default=1)
    album_title = models.CharField(max_length=250)
    description = models.TextField()
    slug = models.SlugField(max_length=200, unique=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    editor = models.CharField(max_length=70)
    cover = models.FileField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False,auto_now_add=True)




    def __str__(self):
        return self.album_title

    def get_absolute_url(self):
        return reverse('show', kwargs={'slug': self.slug})

    class Meta:
        ordering = ['-created']


class Pin(models.Model):
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    slug = models.SlugField(max_length=200, unique=True)
    is_favorite = models.BooleanField(default=False)
    description = models.TextField()
    image = models.FileField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False,auto_now_add=True)



    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('show', kwargs={'slug': self.slug})

    class Meta:
        ordering = ['-timestamp','-created']
