from django.contrib.auth.models import User
from .models import Album, Pin

from django import forms


class AlbumForm(forms.ModelForm):
    class Meta:
        model = Album
        fields = ['editor', 'album_title', 'cover']


class PinForm(forms.ModelForm):
    class Meta:
        model = Pin
        fields = ['name', 
                'is_favorite',
                'image', 
                ]

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
