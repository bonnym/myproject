from django.conf.urls import url
from django.contrib import admin

from . import views

app_name = 'photo'



urlpatterns = [
    url(r'^create/$', views.pin_create, name='create'),

    url(r'^album/$', views.album, name='album'),

    url(r'^register/$', views.register, name='register'),

    url(r'^login_user/$', views.login_user, name='login_user'),

    url(r'^logout_user/$', views.logout_user, name='logout_user'),

    url(r'^(?P<pk>[-\w]+)/$', views.show, name='show'),

]
