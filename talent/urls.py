from django.conf.urls import url
from django.contrib import admin

from . import views

app_name = 'talent'



urlpatterns = [
    url(r'^talent/$', views.talent, name='talent'),
    url(r'^(?P<pk>\d+)/$', views.talent_detail, name='show'),
]
