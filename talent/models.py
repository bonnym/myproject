from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from video.models import Season

class Talent(models.Model):
    name = models.CharField(max_length=200)
    image = models.FileField()
    user = models.ForeignKey(Season, default=1)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False,auto_now_add=True)




    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("talent:detail", kwargs={"id": self.id})

    class Meta:
        ordering = ['-created']
