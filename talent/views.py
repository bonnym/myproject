from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import render
from .models import *

def talent(request):
    context = {
        'talents' : Talent.objects.all(),
    }
    return render(request, 'talent/cast.html', context)

def talent_detail(request, pk):
    talent = get_object_or_404(Talent, pk=pk)
    return render(request, 'talent/talent_detail.html', {'talent': talent})

